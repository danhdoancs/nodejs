module.exports = function(dirPath, extention,callback)
{
var fs = require('fs');
var path = require('path');
fs.readdir(dirPath,function(err, list){
	if(err)
		return callback(err);

	var result = list.filter(function(file){
		return path.extname(file) == '.'+extention;
	});
	return callback(null,result);
});
};
