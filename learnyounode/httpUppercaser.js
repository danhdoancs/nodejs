var http = require('http')
var map = require('through2-map')
var bl = require('bl')

var server = http.createServer(function(req,res){
    if(req.method != 'POST')
        res.end('only accept post')

    req.pipe(map(function(data){
        return data.toString().toUpperCase()
        
    })).pipe(res)       
   
})
server.listen(process.argv[2])
